import Vue from 'vue'
import App from './App.vue'
import './index.css'
import {router} from './router'
import {store} from './store/index.store'
import 'flowbite';
import './index.css'
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
import VueI18n from 'vue-i18n'

Vue.config.productionTip = false

Vue.use(VueI18n)
Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 20,
  newestOnTop: true
});

new Vue({
    render: h => h(App),
    router: router,
    store: store
}).$mount('#app')