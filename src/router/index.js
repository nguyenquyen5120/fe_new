import VueRoute from 'vue-router'
import Vue from 'vue'
Vue.use(VueRoute);

export const router = new VueRoute({
   routes: [
     {
      path: '/',
      name:'home',
      component: ()=>import('@/pages/Home.vue')
     },
     {
      path: '/login',
      name:'login',
      component: ()=>import('@/pages/Login.vue')
     },
     {
      path: '/signup',
      name:'signup',
      component: ()=>import('@/pages/SignUp.vue')
     },
     {
      path: '/reset',
      name:'reset',
      component: ()=>import('@/pages/Reset.vue')
     },
     {
      path: '/reset-password',
      name:'ResetPassword',
      component: ()=>import('@/pages/ResetPassword.vue')
     },
     {
      path: '/verifyAccount',
      name:'verifyAccount',
      component: ()=>import('@/pages/verifyAccount.vue')
     },
     {
      path: '/termsOfService',
      name:'termsOfService',
      component: ()=>import('@/pages/TermOfService.vue')
     },
     {
      path: '/privacy',
      name:'privacy',
      component: ()=>import('@/pages/Privacy.vue')
     },
     {
      path: '/portal',
      name:'portal',
      component: ()=>import('@/pages/Portal.vue')
     },
     {
      path: '/verifyEmail',
      name:'verifyEmail',
      component: ()=>import('@/pages/verifyEmail.vue')
     },
     {
      path: '/notify',
      name:'Notify',
      component: ()=>import('@/pages/Notify.vue')
     },
     {
      path: '/categotyId',
      name:'CategotyId',
      component: ()=>import('@/pages/CategotyId.vue')
     },
     {
      path: '/user',
      name:'user',
      component: ()=>import('@/pages/User.vue')
     },
     {
      path: '*',
      name:'404',
      component: ()=>import('@/pages/404.vue')    
     },
     {
      path: '/categoryQuestion',
      name:'categoryQuestion',
      component: ()=>import('@/pages/CategoryQuestion.vue')    
     },
   ],
   mode:'history'
})
