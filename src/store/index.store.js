import Vue from "vue";
import Vuex from "vuex";
import {homeStore} from "./home.store";
import {cartStore} from "./cart.store";
import {userStore} from "./user.stote"
Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        cart: cartStore,
        home: homeStore,
        user: userStore
    }
});
