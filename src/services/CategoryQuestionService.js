import {Axios} from './Axios';

function getCategoryQuestionById(id) {
   return Axios.get(`/public/categoryQuestion/${id}`);
}
function getAllCategoryQuestion() {
   return Axios.get(`/public/categoryQuestion/getAll`);
}
export const categoryQuestionService = {
    getCategoryQuestionById,
    getAllCategoryQuestion
};