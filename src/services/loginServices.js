import {Axios} from './Axios';

function getSignIn(payload) {
    Axios.setHeaders({
        'content-type': 'application/json'
    })
   return Axios.post(`auth/signin`,payload);
}

function getSignUp(payload) {
    Axios.setHeaders({
        'content-type': 'multipart/form-data'
    })
    const url = "auth/signup";
    return Axios.post(url,payload)
}
function sendEmailResetPassWord(payload) {
    Axios.setHeaders({
        'content-type': 'multipart/form-data'
    })
    return Axios.post(`auth/forgot-password`,payload);
}
function ResetPassword(payload) {
    Axios.setHeaders({
        'content-type': 'multipart/form-data'
    })
    return Axios.post(`auth/forgot-password/reset`,payload);
}
export const loginServices = {
    getSignIn,
    getSignUp,
    sendEmailResetPassWord,
    ResetPassword
};