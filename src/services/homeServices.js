import {Axios} from './Axios';

function getCategory() {
   return Axios.get(`/public/category/getAll/0/16`);
}
function getCategoryById(id) {
   return Axios.post(`/public/category/findById/${id}`);
}
export const homeServices = {
   getCategory,
   getCategoryById
};